<?php

namespace Drupal\westerntoday_migration\Plugin\media_migration\file_entity;

use Drupal\media_migration\Plugin\media_migration\file_entity\RemoteVideoBase;

/**
 * Oembed media migration plugin for Oembed media entities.
 *
 * @FileEntityDealer(
 *   id = "oembed",
 *   types = {"video"},
 *   schemes = {"oembed"},
 *   destination_media_source_plugin_id = "oembed:video"
 * )
 */
class Oembed extends RemoteVideoBase { }
